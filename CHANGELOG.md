## [2.0.0] - 2024-09-16
### Changed
- Raised `wpdesk/wp-wpdesk-license` to v4.0.0 with underlying breaking changes. Check the library's [CHANGELOG](https://gitlab.wpdesk.dev/wpdesk/wp-wpdesk-license/blob/master/CHANGELOG.md) for details.

## [1.0.2] - 2022-05-23
### Reverted
- composer in gitlab CI

## [1.0.1] - 2022-05-22
### Fixed
- composer in gitlab CI

## [1.0.0] - 2022-05-21
### Added
- Init