[![pipeline status](https://gitlab.com/wpdesk/library/wp-plugin-flow-paid/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-plugin-flow-paid/commits/master) 
Integration: [![coverage report](https://gitlab.com/wpdesk/library/wp-plugin-flow-paid/badges/master/coverage.svg?job=integration+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-plugin-flow-paid/commits/master)
Unit: [![coverage report](https://gitlab.com/wpdesk/library/wp-plugin-flow-paid/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-plugin-flow-paid/commits/master)

# wp-plugin-flow-paid

See https://gitlab.com/wpdesk/library/wp-plugin-flow-common

